import React from "react";
import { Layout } from "antd";
import { Link } from "react-router-dom";

export const Footer: React.FC = () => {
  return (
    <Layout.Footer
      style={{
        width: "100%",
        backgroundColor: "#fff",
        height: 20,
        textAlign: "center",
        marginTop: 80,
      }}
    >
       
    </Layout.Footer>
  );
};
